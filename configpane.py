from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QGridLayout, QLineEdit, QLabel, QPushButton, QSizePolicy
from PyQt5.QtGui import QIntValidator
from observables import ObservableDict

class ConfigPane(QGridLayout):
    def __init__(self, params, **kwargs):
        super().__init__(**kwargs)

        # Keep an ObservableDict of the parameter => values here.
        self._configVals = ObservableDict(params)
        self._textinputs = {}

        # Iterate over all config parameters.
        for (i, (p, d)) in enumerate(self._configVals._dict.items()):
            # Make the label and add it.
            label = QLabel(text=p)
            self.addWidget(label, i, 0)
            
            # Save the textinput in a local dict so we can recover it by parameter *name*.
            self._textinputs[p] = QLineEdit(text=str(d))
            self._textinputs[p].setValidator(QIntValidator(1, 100))
            self.addWidget(self._textinputs[p], i, 1)

            # Fancy slot definition using lambda.
            self._textinputs[p].textChanged.connect((lambda p: lambda v: self.on_configure(p, v))(p))

    # This just copies the text from the edit widget into the (observable) dict.
    def on_configure(self, p, v):
        v = 1 if (v == '') else v  # For some reason QIntValidator doesn't handle this.
        self._configVals[p] = int(v)

    # ConfigPane now implements the observable interface. Just passes
    # the slot to the ObservableDict observe() method.
    def observe(self, slot):
        self._configVals.observe(slot)
