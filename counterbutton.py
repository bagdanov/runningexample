from PyQt5.QtWidgets import QPushButton, QApplication
from PyQt5.QtCore import QObject, pyqtSignal
from observables import Observable

class CounterButton(QPushButton):
    '''Simple button widget that counts number of times clicked.'''
    def __init__(self, initval=0, **kwargs):
        super().__init__(**kwargs)

        # The counter value is an observable, so we can connect change
        # signal to a slot.
        self._counter = Observable(0)
        self._counter.observe(self.counterChanged)
        self._counter.value = initval

    # Event callback when mouse is released. Could be done with signal/slot.
    def mouseReleaseEvent(self, event):
        self._counter.value = self._counter.value + 1
        super().mouseReleaseEvent(event)

    def counterChanged(self):
        self.setText('Clicks: {}'.format(self._counter.value))

# Simple test case when run as a script.
if __name__ == '__main__':        
    app = QApplication([])
    app.setStyleSheet('QPushButton {font-size: 30pt}')
    button = CounterButton(10)
    button.show()
    app.exec_()
