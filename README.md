# RunningExample from HCI Laboratory

This is a simple implementation of recent laboratory exercises on
using events (signals/slots) in PyQT5 to manage widget
communication. There are **two** iterations of improvements in this
repository as of now. The first implements `ConfigPane` as an
observable object, and the second implements dynamic updates (thanks
to the students who worked this out during the last laboratory!).

You can access these two different implementations using the git branches
'V1.0' and 'master':

```
git checkout V1.0
```

or 

```
git checkout master
```

