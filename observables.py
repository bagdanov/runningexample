from PyQt5.QtCore import QObject, pyqtSignal

# A simple class implementing observable values.
class Observable(QObject):

    valueChanged = pyqtSignal(object) # The signal used to broadcast changes.
    def __init__(self, initval):
        super().__init__()
        self._value = initval

    @property
    def value(self):
        return self._value

    # The property setter: signal changes after update.
    @value.setter
    def value(self, newval):
        self._value = newval
        self.valueChanged.emit(newval)

    # Register slot to changes in observed values.
    def observe(self, slot):
        self.valueChanged.connect(slot)

# A class implementing observable dict values.
class ObservableDict(QObject):
    valueChanged = pyqtSignal(str, int) # The signal used to broadcast changes.
    def __init__(self, initdict):
        super().__init__()
        self._dict = initdict

    def __len__(self):
        return len(self._dict)
                   
    def __iter__(self):
        return iter(self._dict)
    
    def __getitem__(self, key):
        return self._dict[key]

    # Signal when we change a key.value pair.
    def __setitem__(self, key, value):
        self._dict[key] = value
        self.valueChanged.emit(key, value)

    # Register slot to changes in observed values.
    def observe(self, slot):
        self.valueChanged.connect(slot)

