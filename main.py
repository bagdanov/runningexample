import sys
from counterbutton import CounterButton
from configpane import ConfigPane
from PyQt5.QtWidgets import QPushButton, QApplication, QMainWindow, QSizePolicy
from PyQt5.QtWidgets import QWidget, QGridLayout, QLineEdit, QLabel, QHBoxLayout

# Class to hold a grid of CounterButtons.
class ButtonPane(QGridLayout):
    '''Widget that contains a number of CounterButtons arranged in a GridLayout.'''
    def __init__(self, num_buttons, cols=1, initval=0, **kwargs):
        super().__init__(**kwargs)

        # Logic to create a grid of CounterButtons in cols columns.
        for i in range(num_buttons):
            self.addWidget(CounterButton(initval=initval), i // cols, i % cols)
        
# The main application.
class TestApp(QApplication):
    def __init__(self, args):
        super().__init__(args)

        # The parameter dict.
        self._params = {'Columns': 2, 'Buttons': 10}

        # Root is a QTabWidget, first tab is ConfigPane, second a ButtonPane.
        self._root = QWidget()
        self._rootlayout = QHBoxLayout()
        self._root.setLayout(self._rootlayout)
        self._config = QWidget()
        self._config.setMaximumWidth(100)
        self._config.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self._buttons = QWidget()
        self._configpane = ConfigPane(params=self._params)
        self._config.setLayout(self._configpane)
        self._buttons.setLayout(ButtonPane(num_buttons=10, cols=2))
        self._rootlayout.addWidget(self._config)
        self._rootlayout.addWidget(self._buttons)

        # Call the observe() method on configpane to link to our slot
        self._configpane.observe(self.parameterChanged)

        # And run the app.
        self._root.setFixedSize(640, 480)
        self._root.show()
        self.exec_()

    # This is the slot that is called when a parameter changes.
    def parameterChanged(self, k, v):
        self._rootlayout.itemAt(1).widget().setParent(None)
        self._buttons = QWidget()
        self._buttons.setLayout(ButtonPane(num_buttons=self._params['Buttons'], cols=self._params['Columns']))
        self._rootlayout.addWidget(self._buttons)
        

# Instantiate and run the application.
app = TestApp(sys.argv)
